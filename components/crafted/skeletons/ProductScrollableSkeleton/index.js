import React from 'react';

const ProductScrollableSkeleton = ({loading = false, count = 4}) => {

    return (
        <>
            {
                loading &&
                <div className="product-list" style={{height: 400}}>
                    {Array(count).fill(null).map((value, index) => (
                        <div key={index} className="product-list--item">
                            <div className="skeleton-product">
                                <div className="skeleton skeleton-product-img"></div>
                                <div className="skeleton skeleton-product-price"></div>
                                <div className="skeleton-product-content">
                                    <div className="skeleton skeleton-product-line"></div>
                                    <div className="skeleton skeleton-product-line"></div>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            }
        </>
    );
};

export default ProductScrollableSkeleton;
