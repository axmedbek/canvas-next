import React from 'react';

const CollectionSkeleton = ({loading = false, count = 3}) => {

    return (
        <>
            {
                loading &&
                <div className="collection-list">
                    {Array(count).fill(null).map((value, index) => (
                        <div key={index} className="collection-list--item">
                            <div className="skeleton skeleton-collection"></div>
                        </div>
                    ))}
                </div>
            }
        </>
    );
};

export default CollectionSkeleton;
