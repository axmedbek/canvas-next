import React from 'react';

const ProductGridSkeleton = ({loading = false, count = 4}) => {

    return (
        <>
            {
                loading &&
                <>
                    {Array(count).fill(null).map((value, index) => (
                        <div key={index} className="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-6">
                            <div className="card card-space card-single">
                                <div className="skeleton-product">
                                    <div className="skeleton skeleton-product-img"></div>
                                    <div className="skeleton skeleton-product-price"></div>
                                    <div className="skeleton-product-content">
                                        <div className="skeleton skeleton-product-line"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
                </>
            }
        </>
    );
};

export default ProductGridSkeleton;
