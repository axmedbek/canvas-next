import React, {useState} from 'react';
import SimpleLayout from "../../layouts/SimpleLayout";
import {fetchShopData} from "../../services/common/shop.service";
import {fetchProductData} from "../../services/product.service";
// Import Swiper React components
import {Swiper, SwiperSlide} from 'swiper/react';
import {Thumbs} from 'swiper';

// Import Swiper styles
import 'swiper/css';
import {getImageUrl} from "../../helpers/standard.helper";
import Image from "next/image";

const ProductDetailPage = ({shop, product}) => {
    const [thumbsSwiper, setThumbsSwiper] = useState(null);

    return (
        <SimpleLayout shop={shop}>
            <main className="main-root">
                <div className="wrapper">
                    <div className="page-holder">
                        <div className="product-detail">
                            <div className="row gutter-32">
                                <div className="col-xl-6 col-lg-6 col-12">
                                    <div className="product-detail--carousel">
                                        <div className="product-detail--carousel--container">
                                            <div className="swiper-container product-detail--carousel--thumbs">
                                                <Swiper
                                                    watchSlidesProgress
                                                    spaceBetween={10}
                                                    direction={"vertical"}
                                                    onSwiper={() => console.log('slide swipe')}
                                                    onSlideChange={() => console.log('slide change')}
                                                >
                                                    {
                                                        product.images.map((image, index) => (
                                                            <SwiperSlide key={index}>
                                                                <Image src={getImageUrl(image.src)} alt={product.title}
                                                                       layout={"fill"}
                                                                       style={{
                                                                           objectFit: 'contain',
                                                                           width: '100%'
                                                                       }}/>
                                                            </SwiperSlide>
                                                        ))
                                                    }
                                                </Swiper>
                                            </div>
                                            <div className="swiper-container product-detail--carousel--main">
                                                <div className="swiper-wrapper">
                                                    <Swiper
                                                        watchSlidesProgress
                                                        sliderPerView={5}
                                                        infinite={true}
                                                        onSwiper={() => console.log('slide swipe')}
                                                        onSlideChange={() => console.log('slide change')}
                                                    >
                                                        {
                                                            product.images.map((image, index) => (
                                                                <SwiperSlide key={index}>
                                                                    <Image src={getImageUrl(image.src)}
                                                                           layout={"fill"}
                                                                           alt={product.title} style={{
                                                                        objectFit: 'contain',
                                                                        width: '100%'
                                                                    }}/>
                                                                </SwiperSlide>
                                                            ))
                                                        }
                                                    </Swiper>
                                                </div>
                                                <div className="product-badge">
                                                    <span className="product-badge--discount">-10%</span>
                                                    <span className="product-badge--sale">SALE</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="swiper-pagination"></div>
                                    </div>
                                </div>
                                <div className="col-xl-6 col-lg-6 col-12">
                                    <div className="product-detail--info">
                                        <h1 className="product-detail--title">{product.title}</h1>
                                        <div className="product-detail--price">
                                            <span className="product-detail--price--current">$16.16</span>
                                            <del className="product-detail--price--previous">$98.91</del>
                                        </div>
                                        {
                                            product.sku &&
                                            <span className="product-detail--code">SKU: {product.sku}</span>
                                        }
                                        <div className="product-detail--feature">
                                            <div className="input-wrapper has-icon m-b-24">
                                                <select className="input-field">
                                                    <option disabled selected>Select color</option>
                                                    <option value="0">Blue</option>
                                                    <option value="1">Green</option>
                                                    <option value="2">Pink</option>
                                                </select>
                                                <span className="input-icon ph-caret-down"></span>
                                            </div>
                                            <div className="input-wrapper has-icon m-b-24">
                                                <select className="input-field">
                                                    <option disabled selected>Select size</option>
                                                    <option value="0">XL</option>
                                                    <option value="1">SM</option>
                                                    <option value="2">XS</option>
                                                </select>
                                                <span className="input-icon ph-caret-down"></span>
                                            </div>
                                            <button className="btn btn-fluid btn-md btn-primary"><span
                                                className="btn-icon btn-icon-left ph-shopping-cart-simple"></span>ADD TO
                                                CART
                                            </button>
                                        </div>
                                        <div className="product-detail--description">
                                            <h6 className="product-detail--description--title">DESCRIPTION</h6>
                                            <div dangerouslySetInnerHTML={{__html: product.description}}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <section className="section-product">
                            <div className="section-heading">
                                <h2 className="section-heading--title">Other products</h2>
                            </div>
                            <div className="product-list">

                            </div>
                        </section>
                    </div>
                </div>
            </main>
        </SimpleLayout>
    );
};


export async function getServerSideProps(context) {
    let handle = context.query.handle;

    let shop = await fetchShopData();
    let product = await fetchProductData(handle);

    return {
        props: {
            shop,
            product
        }
    }
}

export default ProductDetailPage;
