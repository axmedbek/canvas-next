import React from 'react';
import SimpleLayout from "../../layouts/SimpleLayout";
import {fetchShopData} from "../../services/common/shop.service";
import {fetchCollectionData} from "../../services/collection.service";
import ProductGridItem from "../../containers/Product/ProductGridItem";
import Link from "next/link";
import {getImageUrl} from "../../helpers/standard.helper";

const CollectionDetailPage = ({shop, collection}) => {
    return (
        <SimpleLayout shop={shop}>
            <main className="main-root">
                <div className="wrapper">
                    <div className="breadcrumb m-t-16">
                        <Link href="/">
                            <a className="breadcrumb-link">Home</a>
                        </Link>
                        <Link href="/collections">
                            <a className="breadcrumb-link" title={"Collections"}>{"Collections"}</a>
                        </Link>
                        <a href="#" className="breadcrumb-link active" title={collection.title}>{collection.title}</a>
                    </div>
                    <section className="section-banner"
                             style={{
                                 backgroundImage: `url(${collection.images[0] ? getImageUrl(collection.images[0].src) : '/static/images/product.png'})`
                             }}>
                        <h1 className="section-banner--heading">{collection.title}</h1>
                    </section>

                    <div className="page-holder">
                        <div className="page-container">
                            <div className="row gutter-32 mobile-gutter-16">
                                {
                                    collection.products.map((product, index) => (
                                        <ProductGridItem key={index} product={product}/>
                                    ))
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </SimpleLayout>
    );
};


export async function getServerSideProps(context) {
    let handle = context.query.handle;

    let shop = await fetchShopData();
    let collection = await fetchCollectionData(handle);

    return {
        props: {
            shop,
            collection
        }
    }
}

export default CollectionDetailPage;
