import SimpleLayout from "../layouts/SimpleLayout";
import HomeBannerContainer from "../containers/Banner/HomeBannerContainer";
import HomeCollectionContainer from "../containers/Collection/HomeCollectionContainer";
import HomeProductContainer from "../containers/Product/HomeProductContainer";
import HomeLeftBannerContainer from "../containers/Banner/HomeLeftBannerContainer";
import HomeRightBannerContainer from "../containers/Banner/HomeRightBannerContainer";
import {fetchShopData} from "../services/common/shop.service";

const HomePage = ({shop}) => {

    return (
        <SimpleLayout shop={shop}>
            <HomeBannerContainer/>
            <HomeCollectionContainer/>
            <HomeProductContainer type={"new"}/>
            <HomeLeftBannerContainer/>
            <HomeProductContainer type={"best"}/>
            <HomeRightBannerContainer/>
        </SimpleLayout>
    );
};

export async function getServerSideProps() {

    let shop = await fetchShopData();

    return {
        props: {
            shop
        }
    }
}

export default HomePage;
