import React from 'react';
import SimpleLayout from "../../layouts/SimpleLayout";
import Link from "next/link";
import {fetchShopData} from "../../services/common/shop.service";

const ForgotPasswordPage = ({ shop }) => {
    return (
        <SimpleLayout shop={shop}>
            <main className="main-root">
                <div className="wrapper">
                    <div className="auth-container">
                        <div className="auth-holder">
                            <h2 className="auth-holder--heading">Reset your password</h2>
                            <div className="auth-holder--form">
                                <div className="input-wrapper m-b-24">
                                    <input type="email" className="input-field" placeholder="Email"/>
                                </div>
                                <button className="btn btn-fluid btn-md btn-primary m-b-24">Submit</button>
                                <Link href={"/account/login"}>
                                    <a className="btn btn-fluid btn-outline-md btn-outline btn-outline-primary">Cancel</a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </SimpleLayout>
    );
};

export async function getServerSideProps() {

    let shop = await fetchShopData();

    return {
        props: {
            shop
        }
    }
}

export default ForgotPasswordPage;
