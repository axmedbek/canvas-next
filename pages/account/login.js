import React from 'react';
import SimpleLayout from "../../layouts/SimpleLayout";
import {fetchShopData} from "../../services/common/shop.service";
import Link from "next/link";

const LoginPage = ({shop}) => {
    return (
        <SimpleLayout shop={shop}>
            <main className="main-root">
                <div className="wrapper">
                    <div className="auth-container">
                        <div className="auth-holder">
                            <h2 className="auth-holder--heading">Login</h2>
                            <div className="auth-holder--form">
                                <div className="input-wrapper has-error m-b-24">
                                    <input type="email" className="input-field" placeholder="Email"/>
                                    <span className="error-message">This field not empty</span>
                                </div>
                                <div className="input-wrapper m-b-8">
                                    <input type="password" className="input-field" placeholder="Password"/>
                                </div>
                                <Link href={"/account/forgot-password"}>
                                    <a className="link link--primary fs-14">Forgot your password?</a>
                                </Link>
                                <button className="btn btn-fluid btn-md btn-primary m-t-24">Sign in</button>
                            </div>
                            <div className="auth-holder--footer">New customer? <Link href={"/account/register"}><a
                                className="link link--primary">Create
                                account</a></Link></div>
                        </div>
                    </div>
                </div>
            </main>
        </SimpleLayout>
    );
};

export async function getServerSideProps() {

    let shop = await fetchShopData();

    return {
        props: {
            shop
        }
    }
}

export default LoginPage;
