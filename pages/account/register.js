import React from 'react';
import SimpleLayout from "../../layouts/SimpleLayout";
import {fetchShopData} from "../../services/common/shop.service";
import Link from "next/link";

const RegisterPage = ({shop}) => {
    return (
        <SimpleLayout shop={shop}>
            <main className="main-root">
                <div className="wrapper">
                    <div className="auth-container">
                        <div className="auth-holder">
                            <h2 className="auth-holder--heading">Create account</h2>
                            <div className="auth-holder--form">
                                <div className="input-wrapper m-b-24">
                                    <input type="text" className="input-field" placeholder="First name"/>
                                </div>
                                <div className="input-wrapper m-b-24">
                                    <input type="text" className="input-field" placeholder="Last name"/>
                                </div>
                                <div className="input-wrapper m-b-24">
                                    <input type="email" className="input-field" placeholder="Email"/>
                                </div>
                                <div className="input-wrapper m-b-24">
                                    <input type="password" className="input-field" placeholder="Password"/>
                                </div>
                                <button className="btn btn-fluid btn-md btn-primary">Create account</button>
                            </div>
                            <div className="auth-holder--footer">Returning customer? <Link href={"/account/login"}><a
                                className="link link--primary">Sign in</a></Link></div>
                        </div>
                    </div>
                </div>
            </main>
        </SimpleLayout>
    );
};

export async function getServerSideProps() {

    let shop = await fetchShopData();

    return {
        props: {
            shop
        }
    }
}

export default RegisterPage;
