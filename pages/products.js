import React, {useEffect} from 'react';
import SimpleLayout from "../layouts/SimpleLayout";
import {fetchShopData} from "../services/common/shop.service";
import {fetchProductListData} from "../services/product.service";
import ProductGridItem from "../containers/Product/ProductGridItem";
import ProductGridSkeleton from "../components/crafted/skeletons/ProductGridSkeleton";

const ProductsPage = ({ shop }) => {

    const [products, setProducts] = React.useState(null);
    const [loading, setLoading] = React.useState(true);

    useEffect(() => {
        setLoading(true);

        fetchProductListData(20).then(response => {
            setProducts(response);
            setLoading(false);
        })
    }, []);

    return (
        <SimpleLayout shop={shop}>
            <main className="main-root">
                <div className="wrapper">
                    <div className="page-holder">
                        <div className="breadcrumb">
                            <a href="#" className="breadcrumb-link">Home</a>
                            <a href="#" className="breadcrumb-link active">All products</a>
                        </div>
                        <h1 className="page-heading">Products</h1>
                        <div className="page-container">
                            <div className="row gutter-32 mobile-gutter-16">
                                <ProductGridSkeleton loading={loading} count={8}/>
                                {
                                    !loading && products && products.map((product, index) => (
                                        <ProductGridItem key={index} product={product} />
                                    ))
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </SimpleLayout>
    );
};


export async function getServerSideProps() {

    let shop = await fetchShopData();

    return {
        props: {
            shop
        }
    }
}
export default ProductsPage;
