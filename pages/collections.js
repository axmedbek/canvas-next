import React, {useEffect} from 'react';
import SimpleLayout from "../layouts/SimpleLayout";
import {fetchShopData} from "../services/common/shop.service";
import CollectionGridItem from "../containers/Collection/CollectionGridItem";
import {fetchCollectionListData} from "../services/collection.service";
import Link from "next/link";
import {useRouter} from "next/router";

const CollectionsPage = ({shop}) => {
    const router = useRouter()
    const { handle } = router.query

    const [collections, setCollectionList] = React.useState(null);

    useEffect(() => {
        fetchCollectionListData(20).then((response) => {
            setCollectionList(response);
        });
    }, []);

    return (
        <SimpleLayout shop={shop}>
            <main className="main-root">
                <div className="wrapper">
                    <div className="page-holder">
                        <div className="breadcrumb">
                            <Link href={"/"}>
                                <a className="breadcrumb-link">Home</a>
                            </Link>
                            <a href="#" className="breadcrumb-link active">Collections</a>
                        </div>
                        <h1 className="page-heading">Collections</h1>
                        <div className="page-container">
                            <div className="row gutter-32">
                                {
                                    collections && collections.map((collection, index) => (
                                        <CollectionGridItem key={index} collection={collection}/>
                                    ))
                                }

                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </SimpleLayout>
    );
};

export async function getServerSideProps() {
    let shop = await fetchShopData();

    return {
        props: {
            shop
        }
    }
}

export default CollectionsPage;
