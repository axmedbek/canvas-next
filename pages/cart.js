import React from 'react';
import SimpleLayout from "../layouts/SimpleLayout";
import Link from "next/link";
import {fetchShopData} from "../services/common/shop.service";

const CartPage = ({ shop }) => {
    return (
        <SimpleLayout shop={shop}>
            <main className="main-root">
                <div className="wrapper">
                    <div className="page-holder">
                        <div className="breadcrumb">
                            <Link href={"/"}>
                                <a className="breadcrumb-link">Home</a>
                            </Link>
                            <a href="#" className="breadcrumb-link active">My cart</a>
                        </div>
                        <h1 className="page-heading">My cart</h1>
                        <div className="page-container">
                            <div className="empty-cart">
                                <h2 className="empty-cart--title">There are no products in the cart.</h2>
                                <div className="empty-cart--action">
                                    <Link href={"/products"}>
                                        <a className="btn btn-width-lg btn-md btn-primary">Continue shopping</a>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </SimpleLayout>
    );
};

export async function getServerSideProps() {

    let shop = await fetchShopData();

    return {
        props: {
            shop
        }
    }
}

export default CartPage;
