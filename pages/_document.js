import Document, {Html, Head, Main, NextScript} from 'next/document'
import React from "react";
import Script from "next/script";

class WebDocument extends Document {
    render() {
        return (
            <Html lang="az-AZ">
                <Head>
                    <title>CANVAS | Uvodo Storefront</title>
                    <meta name="description" content="Uvodo - Storefront"/>
                    <meta name="viewport"
                          content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no"/>
                    <meta name="format-detection" content="telephone=no"/>

                    <link rel="preconnect" href="https://fonts.googleapis.com"/>
                    <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin/>
                    <link
                        href="https://fonts.googleapis.com/css2?family=Lora:wght@400;500;600;700&family=Work+Sans:wght@100;300;400;500;600;700&display=swap"
                        rel="stylesheet"/>

                    <link rel="apple-touch-icon" sizes="180x180" href="/static/images/favicon/apple-touch-icon.png"/>
                    <link rel="icon" type="image/png" sizes="32x32" href="/static/images/favicon/favicon-32x32.png"/>
                    <link rel="icon" type="image/png" sizes="16x16" href="/static/images/favicon/favicon-16x16.png"/>
                    <link rel="manifest" href="/static/images/favicon/site.webmanifest"/>
                    <link rel="mask-icon" href="/static/images/favicon/safari-pinned-tab.svg" color="#000000"/>
                    <meta name="msapplication-TileColor" content="#000000"/>
                    <meta name="theme-color" content="#000000"/>

                    <script src="https://unpkg.com/phosphor-icons" defer></script>
                    <link rel="stylesheet" href="/static/styles/main.css"/>
                </Head>
                <body>
                <Main/>
                <NextScript/>
                </body>
            </Html>
        )
    }
}

export default WebDocument
