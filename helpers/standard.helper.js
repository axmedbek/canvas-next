export function getImageUrl(src){
    return src.startsWith('http') ? src : process.env.NEXT_PUBLIC_API_BASE_URL + src;
}
