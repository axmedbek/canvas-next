import http from "../config/http";

export function fetchCollectionData(slug) {
    let collection = null;

    try {
        collection = http.get('/collections/' + slug);
    } catch (e) {
    }

    return collection;
}


export function fetchCollectionListData(limit = 10) {
    let collections = [];

    try {
        collections = http.get('/collections?limit=' + limit);
    } catch (e) {
    }

    return collections;
}
