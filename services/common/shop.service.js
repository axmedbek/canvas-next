import http from "../../config/http";

export async function fetchShopData() {
    let shop = null;

    try {
        shop = await http.get('/shop');
    } catch (e) {
    }

    return shop;
}
