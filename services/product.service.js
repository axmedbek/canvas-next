import http from "../config/http";

export function fetchProductData(slug) {
    let product = null;

    try {
        product = http.get('/products/' + slug);
    } catch (e) {
    }

    return product;
}

export function fetchProductListData(limit = 10, sort = 'updated_at:desc') {
    let product = null;

    try {
        product = http.get(`/products?limit=${limit}&sort=${sort}`);
    } catch (e) {
    }

    return product;
}

