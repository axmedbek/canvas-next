import axios from 'axios'

const API_URL = process.env.NEXT_PUBLIC_API_BASE_URL;
const NEXT_API_SUFFIX = process.env.NEXT_PUBLIC_API_SUFFIX;
const CLIENT_ID = process.env.NEXT_PUBLIC_CLIENT_ID;

const baseURL = API_URL + NEXT_API_SUFFIX;

const http = axios.create({
    baseURL: baseURL,
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'X-UVODO-CLIENT-ID': CLIENT_ID
    }
})

function authRequestInterceptor(config) {
    if (config.headers && config.url) {
        const isAbsoluteUrl = config.url.startsWith('http')
        const isCrossOrigin = !config.url.startsWith(API_URL)

        // Do not send auth token for cross-origin requests
        if (isAbsoluteUrl && isCrossOrigin) return config

        const token = null;
        // const token = authCookie.getAuthToken()

        if (token) {
            config.headers.authorization = `Bearer ${token}`
        }
    }

    return config
}

http.interceptors.request.use(authRequestInterceptor)
// http.interceptors.response.use(undefined, requestRetryInterceptor)
http.interceptors.response.use((response) => response.data)

export default http

