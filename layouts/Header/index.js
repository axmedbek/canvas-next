import React from 'react';
import Link from "next/link";
import {getImageUrl} from "../../helpers/standard.helper";

const Header = ({shop}) => {

    return (
        <header className="header">
            <div className="wrapper">
                <div className="header-holder">
                    <div className="header-holder--brand">
                        <div className="menu-toggle">
                            <span className="trigger"></span>
                        </div>
                        <Link href="/">
                            <a className="header-holder--logo" title={shop ? shop.name : "Shop"}>
                                <img src={shop && shop.image ? getImageUrl(shop.image.src) : "/static/images/logo.svg"}
                                     className="w-100 h-100 object-fit-cover" alt={shop ? shop.name : "Logo"}/>
                            </a>
                        </Link>
                    </div>
                    <div className="header-holder--navigation">
                        <div className="header-holder--navigation--list" role="navigation">
                            {
                                shop && shop.collections &&
                                shop.collections.map((collection, index) => {
                                    return (
                                        <Link href={`/collections/${collection.handle}`} key={index}>
                                            <a className="header-holder--navigation--link">{collection.title}</a>
                                        </Link>
                                    )
                                })
                            }
                        </div>
                        <div className="mobile--social-list">
                            {
                                shop && shop.facebook &&
                                <a href={shop.facebook} className="mobile--social-link ph-facebook-logo-fill"></a>
                            }
                            {
                                shop && shop.twitter &&
                                <a href={shop.twitter} className="mobile--social-link ph-twitter-logo-fill"></a>
                            }
                            {
                                shop && shop.youtube &&
                                <a href={shop.youtube} className="mobile--social-link ph-youtube-logo-fill"></a>
                            }
                            {
                                shop && shop.instagram &&
                                <a href={shop.instagram} className="mobile--social-link ph-instagram-logo"></a>
                            }
                            {
                                shop && shop.tiktok &&
                                <a href={shop.tiktok} className="mobile--social-link ph-tiktok-logo-fill"></a>
                            }
                            {
                                shop && shop.linkedin &&
                                <a href={shop.linkedin} className="mobile--social-link ph-linkedin-logo-fill"></a>
                            }
                        </div>
                    </div>
                    <div className="header-holder--action">
                        <a href="#" className="header-holder--action--item ph-magnifying-glass search-open"/>
                        <Link href={"/account/login"}>
                            <a className="header-holder--action--item ph-user"/>
                        </Link>
                        <Link href={"/cart"}>
                            <a className="header-holder--action--item ph-shopping-cart-simple"><span
                                className="cart-count">0</span></a>
                        </Link>
                    </div>
                    <div className="search-container">
                        <div className="search-wrapper">
                            <div className="wrapper position-relative">
                                <div className="search-holder">
                                    <div className="search-holder--input">
                                        <div className="input-wrapper has-icon">
                                            <input type="text" className="input-field input-field-lg search-input"
                                                   placeholder="Search"/>
                                            <span className="input-icon input-icon-lg ph-magnifying-glass"></span>
                                        </div>
                                    </div>
                                    <div className="search-dropdown">
                                        <div className="skeleton-search">
                                            <div className="skeleton skeleton-search-img"></div>
                                            <div className="skeleton-search-content">
                                                <div className="skeleton-search-title">
                                                    <div className="skeleton skeleton-search-line"></div>
                                                    <div className="skeleton skeleton-search-line"></div>
                                                </div>
                                                <div className="skeleton skeleton-search-price"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <span className="search-close ph-x-fill"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    );
};

export default Header;
