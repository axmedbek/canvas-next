import React, {useEffect} from 'react';
import Header from "../Header";
import Footer from "../Footer";

const SimpleLayout = ({shop, children}) => {

    const [isActive, setIsActive] = React.useState(false);

    const handleClick = () => {
        setIsActive(!isActive);
    }

    const handleClickOutside = () => {
        setIsActive(false);
    };

    useEffect(() => {
        document.addEventListener('click', handleClickOutside, true);
        return () => {
            document.removeEventListener('click', handleClickOutside, true);
        };
    }, []);

    return (
        <div>
            <Header shop={shop}/>
            {children}
            <Footer shop={shop}/>

            <div className={"float-social-list " + (isActive ? "is-active" : "")}>
                <div className="float-social-list--container">
                    {
                        shop && shop.facebook && <a href={shop.facebook} className="float-social-list--item facebook ph-facebook-logo-fill"></a>
                    }
                    {
                        shop && shop.twitter && <a href={shop.twitter} className="float-social-list--item twitter ph-twitter-logo-fill"></a>
                    }
                    {
                        shop && shop.youtube && <a href={shop.youtube} className="float-social-list--item youtube ph-youtube-logo-fill"></a>
                    }
                    {
                        shop && shop.instagram && <a href={shop.instagram} className="float-social-list--item instagram ph-instagram-logo"></a>
                    }
                    {
                        shop && shop.tiktok && <a href={shop.tiktok} className="float-social-list--item tiktok ph-tiktok-logo-fill"></a>
                    }
                    {
                        shop && shop.linkedin && <a href={shop.linkedin} className="float-social-list--item linkedin ph-linkedin-logo-fill"></a>
                    }
                </div>
                <span onClick={handleClick} className="float-social-list--toggle ph-chats-fill"></span>
            </div>
        </div>
    );
};

export default SimpleLayout;
