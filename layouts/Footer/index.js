import React from 'react';

const Footer = ({ shop }) => {

    return (
        <footer className="footer">
            <div className="footer-main">
                <div className="wrapper">
                    <div className="footer-main--holder">
                        <div className="row gutter-48">
                            <div className="col-xl-4 col-lg-4 col-md-12">
                                <div className="footer-main--column">
                                    <h6 className="footer-main--column--title">ABOUT THE SHOP</h6>
                                    <p className="footer-main--column--slogan">{shop ? shop.about : "About"}</p>
                                    <div className="footer-main--social-list">
                                        {
                                            shop && shop.facebook && <a href={shop.facebook} className="footer-main--social-link ph-facebook-logo-fill"></a>
                                        }
                                        {
                                            shop && shop.twitter && <a href={shop.twitter} className="footer-main--social-link ph-twitter-logo-fill"></a>
                                        }
                                        {
                                            shop && shop.youtube && <a href={shop.youtube} className="footer-main--social-link ph-youtube-logo-fill"></a>
                                        }
                                        {
                                            shop && shop.instagram && <a href={shop.instagram} className="footer-main--social-link ph-instagram-logo"></a>
                                        }
                                        {
                                            shop && shop.tiktok && <a href={shop.tiktok} className="footer-main--social-link ph-tiktok-logo-fill"></a>
                                        }
                                        {
                                            shop && shop.linkedin && <a href={shop.linkedin} className="footer-main--social-link ph-linkedin-logo-fill"></a>
                                        }
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-3 offset-xl-1 col-lg-4 col-md-6">
                                <div className="footer-main--column">
                                    <h6 className="footer-main--column--title">CONTACT US</h6>
                                    <div className="footer-main--column--navigation">
                                    <span className="footer-main--column--navigation--item">
                                        <a href={"mailto:" + (shop ? shop.contact_email : '')} className="footer-main--column--link"><span
                                            className="footer-main--column--link--icon ph-envelope"></span>{shop ? shop.contact_email : ''}</a>
                                    </span>
                                        <span className="footer-main--column--navigation--item">
                                        <a href={"tel:" + (shop && shop.address && shop.address.phone_number ? shop.address.phone_number : '')} className="footer-main--column--link"><span
                                            className="footer-main--column--link--icon ph-phone"></span>{shop && shop.address && shop.address.phone_number ? shop.address.phone_number : ''}</a>
                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-4 col-lg-4 col-md-6">
                                <div className="footer-main--column">
                                    <h6 className="footer-main--column--title">LINKS</h6>
                                    <div className="footer-main--column--navigation">
                                        <div className="row gutter-8">
                                            <div className="col-6">
                                            <span className="footer-main--column--navigation--item">
                                                <a href="#" className="footer-main--column--link">Refund policy</a>
                                            </span>
                                            </div>
                                            <div className="col-6">
                                            <span className="footer-main--column--navigation--item">
                                                <a href="#" className="footer-main--column--link">Privacy policy</a>
                                            </span>
                                            </div>
                                            <div className="col-6">
                                            <span className="footer-main--column--navigation--item">
                                                <a href="#" className="footer-main--column--link">Terms of service</a>
                                            </span>
                                            </div>
                                            <div className="col-6">
                                            <span className="footer-main--column--navigation--item">
                                                <a href="#" className="footer-main--column--link">Shipping policy</a>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="footer-bottom">
                <div className="wrapper">
                    <div className="footer-bottom--holder">
                    <span className="footer-bottom--copyright">Copyright &copy; 2022, CANVAS. Built with ♥ <a
                        href="https://uvodo.com/" target="_blank" className="link link--primary">Uvodo</a></span>
                        <div className="footer-bottom--payment">
                        <span className="footer-bottom--payment--item" title="Visa">
                            <img src="/static/images/payment/visa.svg" alt="visa"/>
                        </span>
                            <span className="footer-bottom--payment--item" title="Mastercard">
                            <img src="/static/images/payment/mastercard.svg" alt="mastercard"/>
                        </span>
                            <span className="footer-bottom--payment--item" title="Amex">
                            <img src="/static/images/payment/amex.svg" alt="amex"/>
                        </span>
                            <span className="footer-bottom--payment--item" title="Discover">
                            <img src="/static/images/payment/discover.svg" alt="discover"/>
                        </span>
                            <span className="footer-bottom--payment--item" title="Maestro">
                            <img src="/static/images/payment/maestro.svg" alt="maestro"/>
                        </span>
                            <span className="footer-bottom--payment--item" title="Paypal">
                            <img src="/static/images/payment/paypal.svg" alt="paypal"/>
                        </span>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    );
};

export default Footer;
