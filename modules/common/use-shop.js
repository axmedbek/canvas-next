import create from 'zustand'

export const useShopStore = create((set) => ({
    shop: undefined,
    getShopData: () => {
        set(() => ({shop: "Ahmad"}))
    }
}))
