import React, {useEffect} from 'react';
import CollectionList from "../CollectionList";
import {fetchCollectionListData} from "../../../services/collection.service";
import CollectionSkeleton from "../../../components/crafted/skeletons/CollectionSkeleton";

const HomeCollectionContainer = () => {

    const [collections, setCollectionList] = React.useState(null);
    const [loading, setLoading] = React.useState(false);

    useEffect(() => {
        setLoading(true);

        fetchCollectionListData(10).then((response) => {
            setCollectionList(response);

            setLoading(false);
        });
    }, []);

    return (
        <section className="section-collection">
            <div className="wrapper">
                <div className="wrapper-sm">
                    <h5 className="heading-5 text-center font-weight-normal m-b-32">We believe beauty is a way of life.
                        Read <a href="/about-us" className="link link--primary">our story</a></h5>
                </div>
                <CollectionSkeleton loading={loading}/>
                {
                    !loading &&
                    <CollectionList collections={collections}/>
                }
            </div>
        </section>
    );
};

export default HomeCollectionContainer;
