import React from 'react';
import Link from "next/link";
import {getImageUrl} from "../../../helpers/standard.helper";
import Image from "next/image";

const CollectionGridItem = ({collection}) => {
    return (
        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
            <div className="collection-card collection-card-space">
                <div className="collection-card-holder">
                    <div className="collection-card-img">
                        <Image
                            src={collection.images[0] ? getImageUrl(collection.images[0].src) : "/static/images/product.png"}
                            layout={"fill"}
                            alt={collection.title} className="w-100 h-100 object-fit-cover"/>
                    </div>
                    <Link href={`/collections/${collection.handle}`}>
                        <a className="collection-card-action" title={collection.title}>
                            <h4 className="collection-card-title">{collection.title}</h4>
                            <span className="collection-card-icon ph-caret-right-fill"></span>
                        </a>
                    </Link>
                </div>
            </div>
        </div>
    );
};

export default CollectionGridItem;
