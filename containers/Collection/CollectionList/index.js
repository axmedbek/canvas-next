import React from 'react';
import CollectionScrollableItem from "../CollectionScrollableItem";

const CollectionList = ({ collections }) => {
    return (
        <div className="collection-list">
            {
                collections && collections.map((collection, index) => (
                    <CollectionScrollableItem key={index} collection={collection}/>
                ))
            }
        </div>
    );
};

export default CollectionList;
