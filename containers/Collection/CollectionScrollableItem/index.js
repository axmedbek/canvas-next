import React from 'react';
import Link from "next/link";
import {getImageUrl} from "../../../helpers/standard.helper";
import Image from "next/image";

const CollectionScrollableItem = ({collection}) => {
    return (
        <div className="collection-list--item">
            <div className="collection-card">
                <div className="collection-card-holder">
                    <div className="collection-card-img">
                        <Image
                            src={collection.images[0]
                                ? getImageUrl(collection.images[0].src)
                                : "/static/images/product.png"}
                            alt={collection.title}
                            className="w-100 h-100 object-fit-cover"
                            layout="fill"
                        />
                    </div>
                    <Link href={`/collections/${collection.handle}`}>
                        <a className="collection-card-action" title={collection.title}>
                            <h4 className="collection-card-title">{collection.title}</h4>
                            <span className="collection-card-icon icon-caret-right"/>
                        </a>
                    </Link>
                </div>
            </div>
        </div>
    );
};

export default CollectionScrollableItem;
