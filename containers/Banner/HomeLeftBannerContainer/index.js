import React from 'react';

const HomeLeftBannerContainer = () => {
    return (
        <section className="section-info section-info--theme-pink">
            <div className="wrapper">
                <div className="section-info--banner left-half"
                     style={{"backgroundImage": 'url(static/images/template/banner/banner-1.webp)'}}>
                </div>
                <div className="section-info--content right-content">
                    <h4 className="section-info--content--title">Sale is On <br/> Up to 50% Off</h4>
                    <p className="section-info--content--subtitle">Score your new favourite outfit for less.</p>
                    <div className="section-info--content--action">
                        <a href="#" className="btn btn-md btn-width-md btn-primary">SHOP NOW <span
                            className="btn-icon btn-icon-right ph-caret-right-fill"></span></a>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default HomeLeftBannerContainer;
