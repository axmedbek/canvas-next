import React from 'react';

const HomeBannerContainer = () => {
    return (
        <section className="section-main" style={{"backgroundImage":"url(static/images/template/bg/main-section-2.webp)"}}>
            <div className="wrapper">
                <div className="section-main--content">
                    <span className="section-main--content--subtitle">NEW KIND OF BEAUTY</span>
                    <h1 className="section-main--content--title">Cruelty free & vegan collection</h1>
                    <div className="section-main--content--action">
                        <a href="/products" className="btn btn-md btn-width-md btn-light">SHOP NOW <span
                            className="btn-icon btn-icon-right ph-caret-right-fill"></span></a>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default HomeBannerContainer;
