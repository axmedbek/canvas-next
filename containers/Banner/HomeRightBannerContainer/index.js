import React from 'react';

const HomeRightBannerContainer = () => {
    return (
        <section className="section-info section-info--theme-orange">
            <div className="wrapper">
                <div className="section-info--banner right-half"
                     style={{"backgroundImage": 'url(static/images/template/banner/banner-2.webp)'}}>
                </div>
                <div className="section-info--content left-content">
                    <span className="section-info--content--category">THE BEST</span>
                    <h4 className="section-info--content--title">In Clean Beauty List</h4>
                    <p className="section-info--content--subtitle">Discover our Bestseller Edit, the best products to
                        restart
                        and refresh your skincare and beauty routine.</p>
                    <div className="section-info--content--action">
                        <a href="#" className="btn btn-md btn-width-md btn-primary">SHOP NOW <span
                            className="btn-icon btn-icon-right ph-caret-right-fill"></span></a>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default HomeRightBannerContainer;
