import React from 'react';
import ProductScrollableItem from "../ProductScrollableItem";

const ProductList = ({ products }) => {
    return (
        <div className="product-list">
            {
                products && products.map((product, index) => (
                    <ProductScrollableItem key={index} product={product}/>
                ))
            }
        </div>
    );
};

export default ProductList;
