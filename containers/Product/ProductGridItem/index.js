import React from 'react';
import Link from "next/link";
import {getImageUrl} from "../../../helpers/standard.helper";
import Image from "next/image";

const ProductGridItem = ({product}) => {
    return (
        <div className="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-6">
            <div className="card card-space card-single">
                <Link href={'/products/' + product.handle}>
                    <a className="card-holder" title={product.title}>
                        <div className="card-holder--img">
                            <Image
                                src={product.images[0] ? getImageUrl(product.images[0].src) : '/static/images/product.png'}
                                layout={"fill"}
                                className="w-100 h-100 object-fit-cover" alt={product.title}/>
                            <div className="product-badge">
                                <span className="product-badge--discount">-10%</span>
                                <span className="product-badge--sale">SALE</span>
                            </div>
                        </div>
                        <div className="card-holder--body">
                            <div className="card-holder--price">
                                <span className="card-holder--price--current">$15.22</span>
                                <del className="card-holder--price--previous">$98.91</del>
                            </div>
                            <h4 className="card-holder--title">{product.title}</h4>
                        </div>
                    </a>
                </Link>
            </div>
        </div>
    );
};

export default ProductGridItem;
