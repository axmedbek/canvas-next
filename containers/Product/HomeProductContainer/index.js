import React, {useEffect} from 'react';
import ProductList from "../ProductList";
import {fetchProductListData} from "../../../services/product.service";
import ProductScrollableSkeleton from "../../../components/crafted/skeletons/ProductScrollableSkeleton";

const HomeProductContainer = ({type = "new"}) => {
    const [products, setProducts] = React.useState(null);
    const [loading, setLoading] = React.useState(true);

    useEffect(() => {
        setLoading(true);

        if (type === "new") {
            fetchProductListData(4).then(response => {
                setProducts(response);
                setLoading(false);
            })
        } else {
            fetchProductListData(4, 'orders_count:desc').then(response => {
                setProducts(response);
                setLoading(false);
            })
        }
    }, []);

    return (
        <section className="section-product">
            <div className="wrapper">
                <div className="section-heading">
                    <h2 className="section-heading--title">{type === "new" ? "New Arrivals" : "Best Sellers"}</h2>
                    <a href={type === "new" ? "/products?sort=updated_at:desc" : "/products?sort=orders_count:desc"}
                       className="section-heading--action">VIEW ALL</a>
                </div>
                <ProductScrollableSkeleton loading={loading}/>
                {
                    !loading &&
                    <ProductList products={products}/>
                }
            </div>
        </section>
    );
};

export default HomeProductContainer;
