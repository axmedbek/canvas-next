document.addEventListener('DOMContentLoaded', function () {
  //Mobile Menu Show 
  var el = document.querySelector('.menu-toggle');
  var elParent = document.querySelector('body');

  el.onclick = function () {
    el.classList.toggle('is-active');
    elParent.classList.toggle('menu-show');
  }; //Mobile Menu Outside Click


  var ignoreClickElement = document.querySelector('.header-holder');
  document.addEventListener('click', function (event) {
    var isClickInsideElement = ignoreClickElement.contains(event.target);

    if (!isClickInsideElement) {
      el.classList.remove('is-active');
      elParent.classList.remove('menu-show');
    }
  }); //Cart Quantity Input

  var value,
      quantity = document.getElementsByClassName('quantity-container');

  function createBindings(quantityContainer) {
    var quantityAmount = quantityContainer.getElementsByClassName('quantity-amount')[0];
    var increase = quantityContainer.getElementsByClassName('increase')[0];
    var decrease = quantityContainer.getElementsByClassName('decrease')[0];
    increase.addEventListener('click', function () {
      increaseValue(quantityAmount);
    });
    decrease.addEventListener('click', function () {
      decreaseValue(quantityAmount);
    });
  }

  function init() {
    for (var i = 0; i < quantity.length; i++) {
      createBindings(quantity[i]);
    }
  }

  ;

  function increaseValue(quantityAmount) {
    value = parseInt(quantityAmount.value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    quantityAmount.value = value;
  }

  function decreaseValue(quantityAmount) {
    value = parseInt(quantityAmount.value, 10);
    value = isNaN(value) ? 0 : value;
    if (value > 1) value--;
    quantityAmount.value = value;
  }

  init(); //Search Dropdown

  var searchField = document.querySelector('.search-input');
  var searchDropdown = document.querySelector('.search-dropdown');
  searchField.addEventListener('input', () => {
    if (searchField.value.length > 1) {
      searchDropdown.classList.add('active');
    } else {
      searchDropdown.classList.remove('active');
    }
  }); //Search Toggle

  var searchOpen = document.querySelector('.search-open');
  var searchInput = document.querySelector('.search-input');
  var searchClose = document.querySelector('.search-close');
  var searchParent = document.querySelector('body');

  searchOpen.onclick = function () {
    searchParent.classList.add('search-show');
    el.classList.remove('is-active');
    elParent.classList.remove('menu-show');
    searchInput.focus();
  };

  searchClose.onclick = function () {
    searchParent.classList.remove('search-show');
    searchDropdown.classList.remove('active');
    searchField.value = '';
  };

  document.onkeydown = function (e) {
    e = e || window.event;

    if (e.keyCode == 27) {
      searchParent.classList.remove('search-show');
      searchDropdown.classList.remove('active');
      searchField.value = '';
    }
  }; //Search Outside Click Close


  var searchContainer = document.querySelector('.search-wrapper');
  var searchOpenButton = document.querySelector('.search-open');
  document.addEventListener('click', function (event) {
    var isClickSearchContainer = searchContainer.contains(event.target);
    var isClickSearchButtonContainer = searchOpenButton.contains(event.target);

    if (!isClickSearchContainer && !isClickSearchButtonContainer) {
      searchParent.classList.remove('search-show');
      searchDropdown.classList.remove('active');
    }
  }); // Product Carousel(Swiper Slider)

  if (document.querySelector('.product-detail')) {
    var thumbsCount = document.querySelectorAll('.thumbsCount .swiper-slide').length;
    var galleryThumbs = new Swiper('.product-detail--carousel--thumbs', {
      centeredSlides: false,
      centeredSlidesBounds: false,
      slidesPerView: thumbsCount,
      watchOverflow: true,
      watchSlidesVisibility: true,
      watchSlidesProgress: true,
      direction: 'vertical'
    });
    var galleryMain = new Swiper('.product-detail--carousel--main', {
      watchOverflow: true,
      watchSlidesVisibility: true,
      watchSlidesProgress: true,
      preventInteractionOnTransition: true,
      effect: 'fade',
      fadeEffect: {
        crossFade: true
      },
      thumbs: {
        swiper: galleryThumbs
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true
      }
    });
    var productContainer = document.querySelector('.product-detail');
    var productDisabledItem = document.querySelector('.product-detail--carousel--main .swiper-slide');

    if (productContainer.classList.contains('disabled')) {
      productDisabledItem.classList.remove('swiper-slide-active');
    }

    galleryMain.on('slideChangeTransitionStart', function () {
      galleryThumbs.slideTo(galleryMain.activeIndex);
    });
    galleryThumbs.on('transitionStart', function () {
      galleryMain.slideTo(galleryThumbs.activeIndex);
    });
  } //Float Social List


  var socialListBtn = document.querySelector('.float-social-list--toggle');
  var socialListContainer = document.querySelector('.float-social-list');

  socialListBtn.onclick = function () {
    socialListContainer.classList.toggle('is-active');
  }; //Float Social List Outside Click


  document.addEventListener('click', function (event) {
    var isClickInsideElement = socialListContainer.contains(event.target);

    if (!isClickInsideElement) {
      socialListContainer.classList.remove('is-active');
    }
  });
});